import random, math, itertools

M = []

I = [1, 2, 3, 4, 5, 6, 7, 8, 9]

P = []

U = []

WIN_COMBOS = [(1, 2, 3), (4, 5, 6), (7, 8, 9), (1, 4, 7), (2, 5, 8), (3, 6, 9),
              (1, 5, 9), (3, 5, 7)]

WIN_COMBOS_EXHAUSTIVE = []

BATCH = 10

Current_Batch = 1

B_W = 0

U_W = 0

class GetOutOfNest( Exception ):
    pass

def recon(sf, M, P, I):
    if len(M)>0:
        ee=0
        while ee < len(M):
            win_strat = True
            e=0
            while e < len(P) and e < len(M[ee]):
                if P[e] == M[ee][e]:
                    pass
                else:
                    win_strat = False
                e+=1
            if win_strat and e+1<len(M[ee]) and M[ee][e+1] and (M[ee][e+1] in I):
                sf = M[ee][e+1]
                return sf
            ee+=1
    
    sf = rando(1, 9)
    return sf

def tinhk(M, P, I):
    sf = recon(None, M, P, I)
    while not (sf in I):
        sf = recon(sf, M, P, I)

    print(sf)
    I.remove(sf)

    P.append(sf)


def user_action(M, U, I):
    sf = rando(1, 9)
    while not (sf in I):
        sf = rando(1, 9)
    
    print(sf)

    I.remove(sf)

    U.append(sf)

    
def rando(x, y):
    return math.floor(x + random.random()*y)


def combo_Yield(a):
    yield tuple(itertools.permutations(itertools.combinations(a,3)))


def perm_Yield(a):
    yield tuple(itertools.permutations(a))


for w in WIN_COMBOS:
    gtap = tuple(perm_Yield(w))
    for e in gtap:
        for ee in e:
            WIN_COMBOS_EXHAUSTIVE.append(ee)

WIN_COMBOS_EXHAUSTIVE = tuple(WIN_COMBOS_EXHAUSTIVE)
print(WIN_COMBOS_EXHAUSTIVE)

while True:
    BOT_WINS = False
    
    I = [1, 2, 3, 4, 5, 6, 7, 8, 9]

    P = []

    U = []

    while len(I)>0:
        try:
            tinhk(M, P, I)
            if(len(P)>2):
                TK = tuple(combo_Yield(P))
                for eee in TK:
                    for ee in eee:
                        for e in ee:
                            if e in WIN_COMBOS_EXHAUSTIVE:
                                I=[]
                                print("Bot Wins! / Draw")
                                BOT_WINS = True
                                raise GetOutOfNest

            if len(I)>0:
                user_action(M, U, I)
                if(len(U)>2):
                    UK = tuple(combo_Yield(U))
                    for eee in UK:
                        for ee in eee:
                            for e in ee:
                                if e in WIN_COMBOS_EXHAUSTIVE:
                                    I=[]
                                    print("You WINRAR!")
                                    BOT_WINS = False
                                    raise GetOutOfNest
        except GetOutOfNest:
            break
                
        print(I, U, P)
    if BOT_WINS:
        #P.append(BOT_WINS)
        M.append(tuple(P))
        B_W+=1
    else:
        #U.append(BOT_WINS)
        M.append(tuple(U))
        U_W+=1
    if Current_Batch%BATCH==0 and str(input(f"Bot's Wins / Draws: {B_W} - User's Wins: {U_W} - Again? "))=='n':
        break
    else:
        print("OK")

    Current_Batch+=1
